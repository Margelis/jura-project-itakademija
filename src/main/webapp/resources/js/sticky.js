var StickyNS = {};

function sticky_relocate() {
	var window_top = $(window).scrollTop();
	if (StickyNS.anchor.length) {
		var anchor_top = StickyNS.anchor.offset().top;
		if (window_top > anchor_top) {
			$('.stickyElement').addClass('stick');
			StickyNS.element_after
					.css('padding-top', StickyNS.element_after_offset);
		} else {
			$('.stickyElement').removeClass('stick');
			StickyNS.element_after.removeAttr("style");
		}
	}
}

$(function() {
	StickyNS.anchor = $('#stickyAnchor');
	StickyNS.element_after = $('.AfterStickyElement');
	if (StickyNS.anchor.length && StickyNS.element_after.length) {
		StickyNS.element_after_offset = (StickyNS.element_after.offset().top - StickyNS.anchor.offset().top).toString() + 'px';
		$(window).scroll(sticky_relocate);
	};
	sticky_relocate();
});