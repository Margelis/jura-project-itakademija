package com.infobalt.utilities;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;


public class ImageUtility {

	public static byte[] resizeToFitSquareContainer(byte[] fileData, int sideLength) throws IOException {
    	ByteArrayInputStream in = new ByteArrayInputStream(fileData);
    	try {
    		BufferedImage img = ImageIO.read(in);
    		int height = img.getHeight();
    		int width = img.getWidth();
    		if (width < height) {
    			width = sideLength * width / height;
    			height = sideLength;
    		} else {
    			height = sideLength * height / width;
    			width = sideLength;
    		}
    		Image scaledImage = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
    		BufferedImage imageBuff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    		imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(0,0,0), null);

    		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

    		ImageIO.write(imageBuff, "jpg", buffer);

    		return buffer.toByteArray();
    	} catch (IOException e) {
    		throw new IOException("Scale was unsuccesful.");
    	}
    }
}
