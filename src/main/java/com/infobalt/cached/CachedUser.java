package com.infobalt.cached;

import com.infobalt.services.UserService;
import com.infobalt.entities.User;

import java.util.List;


public class CachedUser {

	UserService userService;
	private List<User> users;

	public List<User> getUsers() {
		if (users == null) {
			users = userService.findAll();
		}
		return users;
	}
	
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}
