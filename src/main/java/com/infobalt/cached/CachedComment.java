package com.infobalt.cached;

import java.util.List;

import com.infobalt.entities.Comment;
import com.infobalt.entities.Task;
import com.infobalt.services.CommentService;

public class CachedComment {

	private CommentService commentService;
	private List<Comment> comments;
	
	public CommentService getCommentService() {
		return commentService;
	}
	
	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}
	
	public List<Comment> getComments(Task task) {
		if(comments == null){
			comments = commentService.findByTask(task);
		}
		return comments;
	}
	
	public void reset(){
		comments = null;
	}
	
}
