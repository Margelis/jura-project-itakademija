package com.infobalt.cached;

import java.util.List;

import com.infobalt.entities.Task;
import com.infobalt.services.AttachmentService;
import com.infobalt.transfer_objects.AttachmentDetails;

public class CachedAttachment {

	private AttachmentService attachmentService;
	private List<AttachmentDetails> attachmentDetailsList;
	
	public List<AttachmentDetails> findAttchmentDetailsListByTask(Task task) {
		if (attachmentDetailsList == null) {
			attachmentDetailsList = attachmentService.findAttchmentDetailsListByTask(task);
		}
		return attachmentDetailsList;
	}
	
	public AttachmentService getAttachmentService() {
		return attachmentService;
	}
	
	public void setAttachmentService(AttachmentService attachmentService) {
		this.attachmentService = attachmentService;
	}
	
}
