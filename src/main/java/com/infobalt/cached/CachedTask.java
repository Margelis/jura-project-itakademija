package com.infobalt.cached;

import java.util.List;

import com.infobalt.entities.Project;
import com.infobalt.entities.Sprint;
import com.infobalt.entities.Task;
import com.infobalt.entities.User;
import com.infobalt.enums.TaskStatus;
import com.infobalt.services.TaskService;

public class CachedTask {

	TaskService taskService;
	private List<Task> tasks;
	private List<Task> tasksToDo;
	private List<Task> tasksInProgress;
	private List<Task> tasksDone;
	private List<Task> tasksInSprint;
	private List<Task> suspendedUsersTasks;
	private List<Task> foundTasks;
	private List<Task> backlogTasks;
	
	public List<Task> getUserTasks(User user) {
		if (tasks == null) {
			tasks = taskService.findUserTasks(user);
		}
		return tasks;
	}
	
	public List<Task> getProjectTasks(Project project) {
		if (tasks == null) {
			tasks = taskService.findByProject(project);
		}
		return tasks;
	}

	public List<Task> findSuspendedUsersTasksInProject(Project project){
		if(suspendedUsersTasks == null){
			suspendedUsersTasks = taskService.findSuspendedUsersTasksInProject(project);
		}
		return suspendedUsersTasks;
	}
	
	public List<Task> findBySprintAndStatus(Sprint sprint, TaskStatus taskStatus){
		if (tasks == null){
			tasks = taskService.findBySprintAndStatus(sprint, taskStatus);
		}
		return tasks;
	}
	
	public List<Task> getTasksToDo(Sprint sprint){
		if (tasksToDo == null){
			tasksToDo = taskService.findBySprintAndStatus(sprint, TaskStatus.TODO);
		}
		return tasksToDo;
	}
	
	public List<Task> getTasksInProgress(Sprint sprint){
		if (tasksInProgress == null){
			tasksInProgress = taskService.findBySprintAndStatus(sprint, TaskStatus.INPROGRESS);
		}
		return tasksInProgress;
	}
	
	public List<Task> getTasksDone(Sprint sprint){
		if (tasksDone == null){
			tasksDone = taskService.findBySprintAndStatus(sprint, TaskStatus.DONE);
		}
		return tasksDone;
	}
	
	public List<Task> getTasksNotInSprint(Project project){
		if (backlogTasks == null) {
			backlogTasks = taskService.findNotInSprint(project);
		}
		return backlogTasks;
	}
	
	public List<Task> getTasksInSprint(Sprint sprint){
		if (tasksInSprint == null) {
			tasksInSprint = taskService.findBySprint(sprint);
		}
		return tasksInSprint;
	}
	
	public List<Task> getTasksFoundByFragment(String fragment, Project project){
		if (foundTasks == null) {
			foundTasks = taskService.findByDescriptionFragment(fragment, project);
		}
		return foundTasks;
	}
		
	public void reset(){
		tasks = null;
		tasksDone = null;
		tasksInProgress = null;
		tasksToDo = null;
		tasksInSprint = null;
		suspendedUsersTasks = null;
		foundTasks = null;
		backlogTasks = null;
	}
	
	public TaskService getTaskServicee() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

}
