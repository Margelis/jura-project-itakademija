package com.infobalt.transfer_objects;

public class AttachmentDetails {
	private Integer id;
	private String name;
	private Integer size;
	private String mimeType;
	
	public AttachmentDetails(Integer id, String name, Integer size, String mimeType) {
		super();
		this.id = id;
		this.name = name;
		this.size = size;
		this.mimeType = mimeType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getSize() {
		return size;
	}
	
	public void setSize(Integer size) {
		this.size = size;
	}
	
	public String getMimeType() {
		return mimeType;
	}
	
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	
}
