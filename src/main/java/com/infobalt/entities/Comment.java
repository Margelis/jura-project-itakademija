package com.infobalt.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Comment implements Serializable{

	private static final long serialVersionUID = 8955086001958549614L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)//patikslint
	@Column(name = "ID")
	private Integer id;
	@ManyToOne
	private User user;
	@ManyToOne
	private Task task;
	private Date commentDate;
	private String comment;
	
	public Comment(){};
	public Comment(User user, Task task, Date commentDate, String comment) {
		super();
		this.user = user;
		this.task = task;
		this.commentDate = commentDate;
		this.comment = comment;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Task getTask() {
		return task;
	}
	public void setTask(Task task) {
		this.task = task;
	}
	public Date getCommentDate() {
		return commentDate;
	}
	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
}
