package com.infobalt.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Project implements Serializable{

	private static final long serialVersionUID = -5028936337726579304L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@OneToOne (fetch = FetchType.EAGER)
	private User projectManager;
	
	@NotNull
	@Size(min=2, max=32, message="Title must be between 2 and 32 characters.")
	@Column(length=32)
	private String title;
	
	private String description;
	
	private Date startDate;

	private Date finishDate;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable( 
		      name="USER_PROJECTS",
		      joinColumns={@JoinColumn(name="PROJECT_ID", referencedColumnName="ID")},
		      inverseJoinColumns={@JoinColumn(name="USER_ID", referencedColumnName="ID")})
	private List<User> users;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public Date getFinishDate() {
		return finishDate;
	}
	
	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}
	
	public List<User> getUsers() {
		return users;
	}
	
	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	public User getProjectManager() {
		return projectManager;
	}
	
	public void setProjectManager(User projectManager) {
		this.projectManager = projectManager;
	}
	
}
