package com.infobalt.services;

import java.util.Date;
import java.util.List;

import com.infobalt.entities.Project;
import com.infobalt.entities.Sprint;
import com.infobalt.entities.Task;
import com.infobalt.enums.TaskStatus;
import com.infobalt.repositories.SprintDao;

public class SprintService {
	
	private SprintDao sprintDao;
	private TaskService taskService;
	
	public void save(Sprint sprint){
		sprintDao.save(sprint);
	}
	
	public void delete(Sprint sprint){
		sprintDao.delete(sprint);
	}
	
	public List<Sprint> findAll(){
		return sprintDao.findAll();
	}
	
	public List<Sprint> findByProject(Project project){
		return sprintDao.findByProject(project);
	}
	
	public List<Sprint> findSprintsByNamePattern(Project project, String query){
		return sprintDao.findSprintsByNamePattern(project, query);
	}
	
	public Sprint findById(Integer id){
		return sprintDao.findById(id);
	}
	
	/**
	 * Returns true if sprint is finished, but has unfinished tasks.
	 * Returns false if everything is finished, or sprint is not finished yet, or null was passed through as a parameter.
	 * @param sprint Sprint to be checked
	 * @return boolean Sprint failed
	 */
	public boolean isSprintFailed(Sprint sprint){
		if (sprint == null || sprint.getFinishDate() == null){
			return false;
		}
		Date finish = sprint.getFinishDate();
		Date now = new Date();
		List<Task> tasksInProgress = taskService.findBySprintAndStatus(sprint, TaskStatus.INPROGRESS);
		List<Task> tasksToDo = taskService.findBySprintAndStatus(sprint, TaskStatus.TODO);
		List<Task> tasksDone = taskService.findBySprintAndStatus(sprint, TaskStatus.DONE);
		if (now.getTime() > finish.getTime() && (tasksInProgress.size() > 0 || tasksToDo.size() > 0)){
			return true;
		}
		for(Task task: tasksDone){
			if (task.getCompleteDate().getTime() > sprint.getFinishDate().getTime()){
				return true;
			}
		}
		return false;
	}	

	public SprintDao getSprintDao() {
		return sprintDao;
	}

	public void setSprintDao(SprintDao sprintDao) {
		this.sprintDao = sprintDao;
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}
	
}
