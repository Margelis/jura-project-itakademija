package com.infobalt.services;

import java.util.Date;
import java.util.List;

import com.infobalt.entities.Project;
import com.infobalt.entities.Sprint;
import com.infobalt.entities.Task;
import com.infobalt.entities.User;
import com.infobalt.enums.TaskStatus;
import com.infobalt.repositories.TaskDao;

public class TaskService {
	
	private TaskDao taskDao;
	
	/**
	 * Changes task's update date to current date
	 * and passes the task to DAO save method
	 * @param task - a task to be saved
	 */
	public void save(Task task){
		task.setUpdateDate(new Date());
		taskDao.save(task);
	}
	
	public void delete(Task task){
		taskDao.delete(task);
	}
	
	public void removeSprint(Task task){
		task.setSprint(null);
		task.setUpdateDate(new Date());
		save(task);
	}
	
	/**
	 * Adds task to a sprint;
	 * sets AddedToSprintDate to current date;
	 * calls save method
	 * @param task - task to be added
	 * @param sprint - sprint to which task will be added
	 */
	public void addToSprint(Task task, Sprint sprint){
		task.setSprint(sprint);
		task.setUpdateDate(new Date());
		task.setAddedToSprintDate(new Date());
		save(task);
	}
	
	/**
	 * If task status is changed to INPROGRESS, startDate is set to current date,
	 * if task status is changed to DONE, completeDate is set to current date, competedBy set to user, who completed task
	 * if task status is changed to TODO again, startDate and completeDate are set to null;
	 * Calls this.save(task).
	 * @param task - a task to be saved
	 * @param user - user, who changed task status
	 */
	public void changeStatus(Task task, User user){
		if (task.getTaskStatus() == TaskStatus.INPROGRESS){
			task.setStartDate(new Date());
			task.setCompleteDate(null);
			task.setCompletedBy(null);
		}
		if (task.getTaskStatus() == TaskStatus.DONE){
			task.setCompleteDate(new Date());
			task.setCompletedBy(user);
		}
		if (task.getTaskStatus() == TaskStatus.TODO){
			task.setCompleteDate(null);
			task.setStartDate(null);
			task.setCompletedBy(null);
		}
		this.save(task);
	}
	
	public List<Task> findAll(){
		return taskDao.findAll();
	}
	
	public List<Task> findUserTasks(User user){
		return taskDao.findUserTasks(user);
	}
	
	public Task findById(Integer id){
		return taskDao.findById(id);
	}
	
	public List<Task> findByProject(Project project){
		return taskDao.findByProject(project);
	}
	
	public List<Task> findBySprint(Sprint sprint){
		return taskDao.findBySprint(sprint);
	}
	
	public List<Task> findByProjectAndStatus(Project project, TaskStatus taskStatus){
		return taskDao.findByProjectAndStatus(project, taskStatus);
	}
	
	public List<Task> findBySprintAndStatus(Sprint sprint, TaskStatus taskStatus){
		return taskDao.findBySprintAndStatus(sprint, taskStatus);
	}
	
	public List<Task> findNotInSprint(Project project){
		return taskDao.findNotInSprint(project);
	}
	
	public List<Task> findByDescriptionFragment(String fragment, Project project){
		return taskDao.findByDescriptionFragment(fragment, project);
	}
	
	public List<Task> findByProjectAndUserWithStatusNotDone(Project project, User user){
		return taskDao.findByProjectAndUserWithStatusNotDone(project, user);
	}
	
	/**
	 * Finds tasks with status TODO and INPROGRESS, assigned to suspended users, who participates in project.
	 * @param project project
	 * @return tasks task list
	 */
	public List<Task> findSuspendedUsersTasksInProject(Project project){
		return	taskDao.findSuspendedUsersTasksInProject(project);
	}

	/**
	 * Counts number of tasks in current project
	 * @return int Number of tasks
	 */
	public int countTasks(Project project){
		int count = findByProject(project).size();
		return count;
	}
	
	/**
	 * Counts number of tasks with taskStatus.DONE in current project
	 * @param Project project
	 * @return int Number of tasks
	 */
	public int countDoneTasks(Project project){
		int count = findByProjectAndStatus(project, TaskStatus.DONE).size();
		return count;
	}
	
	/**
	 * Counts number of tasks where taskStatus is not equal to DONE in current project
	 * @param Project project
	 * @return int Number of tasks
	 */
	public int countTasksLeft(Project project) {
		return countTasks(project) - countDoneTasks(project);
	}
	
	public TaskDao getTaskDao() {
		return taskDao;
	}

	public void setTaskDao(TaskDao taskDao) {
		this.taskDao = taskDao;
	}
	
}
