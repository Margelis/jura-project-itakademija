package com.infobalt.services;

import java.util.List;

import com.infobalt.entities.Attachment;
import com.infobalt.entities.Task;
import com.infobalt.repositories.AttachmentDao;
import com.infobalt.transfer_objects.AttachmentDetails;

public class AttachmentService {
	
	AttachmentDao attachmentDao;

	public AttachmentDao getAttachmentDao() {
		return attachmentDao;
	}

	public void setAttachmentDao(AttachmentDao attachmentDao) {
		this.attachmentDao = attachmentDao;
	}
	
	public void save(Attachment attachment) {
		attachmentDao.save(attachment);
	}

	public List<AttachmentDetails> findAttchmentDetailsListByTask(Task task) {
		return attachmentDao.findAttchmetDetailsListByTask(task);
	}
	
	public Attachment findById(Integer id) {
		return attachmentDao.findById(id);
	}

}
