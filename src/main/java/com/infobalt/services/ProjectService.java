package com.infobalt.services;

import java.util.Date;
import java.util.List;

import com.infobalt.entities.Project;
import com.infobalt.entities.Task;
import com.infobalt.entities.User;
import com.infobalt.enums.TaskStatus;
import com.infobalt.repositories.ProjectDao;

public class ProjectService {
	
	private ProjectDao projectDao;
	private TaskService taskService;
	
	public List<Project> findAll(){
		return projectDao.findAll();
	}
	
	public List<Project> findUserManagedProjects(User user){
		return projectDao.findUserManagedProjects(user);
	}

	public List<Project> getUserProjects(User user){
		return projectDao.getUserProjects(user);
	}
	
	public void save(Project project){
		projectDao.save(project);
	}
	
	public void delete(Project project){
		projectDao.delete(project);
	}
	
	public Project findById(Integer id){
		return projectDao.findById(id);
	}
	
	/**
	 * Returns progress of current project by calculating ratio of story points
	 * sum of tasks with status DONE and tasks with statuses INPROGRESS and TODO
	 * @param Project project
	 * @return double Progress
	 */
	public double getProgress(Project project) {
		try {
			int storyPointsDone = countStoryPointsDone(project);
			int storyPointsLeft = countStoryPointsLeft(project);
			double result = (double) (storyPointsDone) / (double) (storyPointsDone + storyPointsLeft);
			
			if(!Double.isNaN(result)){
				return result;
			}else{
				return 0;
			}
		} catch (ArithmeticException e) {
			return 0;
		}
	}
	
	/**
	 * Returns working days, that is the number of days between the start date
	 * and finish date or current date, depending whether project is finished or
	 * still in progress
	 * @param Project project
	 * @return int project duration in days
	 */
	public int workingTime(Project project) {
		try {
			Date d1 = project.getStartDate();
			Date d2 = project.getFinishDate();
			Date dCurrent = new Date();
			int days;
			if (dCurrent.getTime() <= d2.getTime()) {
				days = (int) ((dCurrent.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
			} else
				days = (int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
			return days;
		} catch (NullPointerException e) {
			return 0;
		}
	}
	
	/**
	 * Calculates task velocity of tasks accomplishment in current project.
	 * @param Project project
	 * @return double Velocity
	 */
	public double getTasksPerDay(Project project) {
		try {
			return (double) taskService.countDoneTasks(project) / (double) workingTime(project);
		} catch (ArithmeticException e) {
			return 0.0;
		}
	}
	
	/**
	 * Calculates task velocity of story points accomplishment in current
	 * project.
	 * @param Project project
	 * @return double Velocity
	 */
	public double getStoryPointsPerDay(Project project) {
		try {
			return (double) countStoryPointsDone(project) / (double) workingTime(project);
		} catch (ArithmeticException e) {
			return 0.0;
		}
	}
	
	/**
	 * Calculates number of done story points in current project
	 * @param Project project
	 * @return int Story points
	 */
	public int countStoryPointsDone(Project project) {
		int storyPoints = 0;
		List<Task> tasks = taskService.findByProjectAndStatus(project, TaskStatus.DONE);
		for (Task task : tasks) {
			storyPoints = storyPoints + task.getStoryPoints();
		}
		return storyPoints;
	}
	
	/**
	 * Calculates number of story points still left to be done in current project
	 * @param Project project
	 * @return int Story points
	 */
	public int countStoryPointsLeft(Project project) {
		int storyPoints = 0;
		List<Task> tasks = taskService.findByProjectAndStatus(project, TaskStatus.INPROGRESS);
		for (Task task : tasks) {
			storyPoints = storyPoints + task.getStoryPoints();
		}
		List<Task> tasks2 = taskService.findByProjectAndStatus(project, TaskStatus.TODO);
		for (Task task : tasks2) {
			storyPoints = storyPoints + task.getStoryPoints();
		}
		return storyPoints;
	}
	
	public ProjectDao getProjectDao() {
		return projectDao;
	}
	
	public void setProjectDao(ProjectDao projectDao) {
		this.projectDao = projectDao;
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}
	
}
