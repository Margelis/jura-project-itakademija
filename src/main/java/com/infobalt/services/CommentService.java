package com.infobalt.services;

import java.util.List;

import com.infobalt.entities.Comment;
import com.infobalt.entities.Task;
import com.infobalt.repositories.CommentDao;

public class CommentService {
	
	private CommentDao commentDao;
	
	public void save(Comment comment){
		commentDao.save(comment);
	}
	
	public void delete(Comment comment){
		commentDao.delete(comment);
	}
	
	public List<Comment> findAll(){
		return commentDao.findAll();
	}
	
	public Comment findById(Integer id){
		return commentDao.findById(id);
	}

	public CommentDao getCommentDao() {
		return commentDao;
	}

	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}
	
	public List<Comment> findByTask(Task task) {
		return commentDao.findByTask(task);
	}
	
}
