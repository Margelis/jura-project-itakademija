package com.infobalt.services;

import java.util.List;

import com.infobalt.entities.Role;
import com.infobalt.repositories.RoleDao;

public class RoleService {
	private RoleDao roleDao;
	
	public Role getRole(Integer id){
		return roleDao.getRole(id);
	}
	
	public List<Role> findAll() {
		return roleDao.findAll();
	}

	public RoleDao getRoleDao() {
		return roleDao;
	}

	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}
	
	
}
