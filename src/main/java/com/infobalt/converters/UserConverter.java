package com.infobalt.converters;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import com.infobalt.entities.User;
import com.infobalt.services.UserService;


public class UserConverter implements Converter {

	UserService userService;

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String submittedValue) {
		if (submittedValue == null || submittedValue.isEmpty()) {
	        return null;
	    }

	    try {
	        return userService.findById(Integer.valueOf(submittedValue));
	    } catch (NumberFormatException e) {
	        throw new ConverterException(new FacesMessage(submittedValue + " is not a valid User ID"), e);
	    }
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object modelValue) {
		if (modelValue == null) {
	        return "";
	    }

	    if (modelValue instanceof User) {
	        return String.valueOf(((User) modelValue).getId());
	    } else {
	        throw new ConverterException(new FacesMessage(modelValue + " is not a valid User"));
	    }
	}

}
