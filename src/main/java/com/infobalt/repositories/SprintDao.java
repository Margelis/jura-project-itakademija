package com.infobalt.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.infobalt.entities.Project;
import com.infobalt.entities.Sprint;

public class SprintDao {
	private EntityManagerFactory entityManagerFactory;
	private EntityManager em;

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
		this.em = entityManagerFactory.createEntityManager();
	}

	public void save(Sprint sprint) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		if (!em.contains(sprint))
			sprint = em.merge(sprint);
		em.persist(sprint);
		em.getTransaction().commit();
		em.close();
	}

	public void delete(Sprint sprint) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		sprint = em.merge(sprint);
		em.remove(sprint);
		em.getTransaction().commit();
		em.close();
	}
	
	public List<Sprint> findAll() {
		em = entityManagerFactory.createEntityManager();
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Sprint> cq = cb.createQuery(Sprint.class);
			Root<Sprint> root = cq.from(Sprint.class);
			cq.select(root); // we select entity here
			TypedQuery<Sprint> q = em.createQuery(cq);
			return q.getResultList();
		} finally {
			em.close();
		}
	}
	
	public Sprint findById(Integer id) {
		em = entityManagerFactory.createEntityManager();
		TypedQuery<Sprint> query = em.createQuery("SELECT s From Sprint s WHERE s.id = :id", Sprint.class);
		query.setParameter("id", id);
		query.setMaxResults(1);
		return query.getSingleResult();
	}
	
	public List<Sprint> findByProject(Project project) {
		em = entityManagerFactory.createEntityManager();
		TypedQuery<Sprint> query = em.createQuery("SELECT s From Sprint s WHERE s.project = :project", Sprint.class);
		query.setParameter("project", project);
		return query.getResultList();
	}
	
	public List<Sprint> findSprintsByNamePattern(Project project, String query) {
		em = entityManagerFactory.createEntityManager();
		try {
			TypedQuery<Sprint> q = em.createQuery(
					"SELECT s FROM Sprint s WHERE s.project = :project AND LOWER(s.title) LIKE :pattern", Sprint.class);
			q.setParameter("pattern", "%" + query.toLowerCase() + "%");
			q.setParameter("project", project);
			return q.getResultList();
		} finally {
			em.close();
		}
	}
	
}
