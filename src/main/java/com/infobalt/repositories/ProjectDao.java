package com.infobalt.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.infobalt.entities.Project;
import com.infobalt.entities.User;

public class ProjectDao{

	private EntityManagerFactory entityManagerFactory;
	private EntityManager em;

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
		this.em = entityManagerFactory.createEntityManager();
	}

	public void save(Project newProject) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		if (!em.contains(newProject))
			newProject = em.merge(newProject);
		em.persist(newProject);
		em.getTransaction().commit();
		em.close();
	}

	public void delete(Project project) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		project = em.merge(project);
		em.remove(project);
		em.getTransaction().commit();
		em.close();
	}
	
	public List<Project> findUserManagedProjects(User user){
		em = entityManagerFactory.createEntityManager();
		try{
			TypedQuery<Project> projectQuery = em.createQuery("SELECT p From Project p WHERE p.projectManager = :projectManager", Project.class);
			projectQuery.setParameter("projectManager", user);
			return projectQuery.getResultList();
		} finally{
			em.close();
		}
	}
	
	public List<Project> getUserProjects(User user){
		em = entityManagerFactory.createEntityManager();
		try{
			TypedQuery<Project> projectQuery = em.createQuery("SELECT p From Project p JOIN p.users u WHERE u.id = :user", Project.class);
			projectQuery.setParameter("user", user.getId());
			return projectQuery.getResultList();
		} finally{
			em.close();
		}
	}

	public List<Project> findAll() {
		em = entityManagerFactory.createEntityManager();
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Project> cq = cb.createQuery(Project.class);
			Root<Project> root = cq.from(Project.class);
			cq.select(root); // we select entity here
			TypedQuery<Project> q = em.createQuery(cq);
			return q.getResultList();
		} finally {
			em.close();
		}
	}
	
	public Project findById(Integer id) {
		em = entityManagerFactory.createEntityManager();
		TypedQuery<Project> projectQuery = em.createQuery("SELECT p From Project p WHERE p.id = :id", Project.class);
		projectQuery.setParameter("id", id);
		projectQuery.setMaxResults(1);
		return projectQuery.getSingleResult();

	}
}
