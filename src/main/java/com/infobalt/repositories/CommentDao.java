package com.infobalt.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.infobalt.entities.Comment;
import com.infobalt.entities.Task;

public class CommentDao {
	
	private EntityManagerFactory entityManagerFactory;
	private EntityManager em;

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
		this.em = entityManagerFactory.createEntityManager();
	}

	public void save(Comment newComment) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		if (!em.contains(newComment))
			newComment = em.merge(newComment);
		em.persist(newComment);
		em.getTransaction().commit();
		em.close();
	}

	public void delete(Comment comment) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		comment = em.merge(comment);
		em.remove(comment);
		em.getTransaction().commit();
		em.close();
	}

	public List<Comment> findAll() {
		em = entityManagerFactory.createEntityManager();
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Comment> cq = cb.createQuery(Comment.class);
			Root<Comment> root = cq.from(Comment.class);
			cq.select(root); // we select entity here
			TypedQuery<Comment> q = em.createQuery(cq);
			return q.getResultList();
		} finally {
			em.close();
		}
	}
	
	public Comment findById(Integer id) {
		em = entityManagerFactory.createEntityManager();
		TypedQuery<Comment> query = em.createQuery("SELECT c From Comment c WHERE c.id = :id", Comment.class);
		query.setParameter("id", id);
		query.setMaxResults(1);
		return query.getSingleResult();

		
	}
	public List<Comment> findByTask(Task task) {
		em = entityManagerFactory.createEntityManager();
		TypedQuery<Comment> query = em.createQuery("SELECT c From Comment c WHERE c.task.id = :id ORDER BY c.commentDate DESC", Comment.class);
		query.setParameter("id", task.getId());
		return query.getResultList();

	}
}
