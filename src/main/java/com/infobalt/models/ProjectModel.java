package com.infobalt.models;

import com.infobalt.entities.Project;

public class ProjectModel {

	private Project currentProject;

	public Project getCurrentProject() {
		return currentProject;
	}

	public void setCurrentProject(Project currentProject) {
		this.currentProject = currentProject;
	}

}
