package com.infobalt.models;

import org.primefaces.model.StreamedContent;

import com.infobalt.entities.Attachment;

public class AttachmentModel {

	private Attachment currentAttachment;
	private StreamedContent downloadFile;

	public Attachment getCurrentAttachment() {
		return currentAttachment;
	}

	public void setCurrentAttachment(Attachment currentAttachment) {
		this.currentAttachment = currentAttachment;
	}

	public StreamedContent getDownloadFile() {
		return downloadFile;
	}
	
	public void setDownloadFile(StreamedContent downloadFile) {
		this.downloadFile = downloadFile;
	}
}
