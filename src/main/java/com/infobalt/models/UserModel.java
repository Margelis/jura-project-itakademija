package com.infobalt.models;

import org.springframework.security.core.context.SecurityContextHolder;

import com.infobalt.entities.User;
import com.infobalt.security.UserPrincipal;

public class UserModel {

	private User currentUser; // Temporary user for UI admin crud operations with User entities

	/**
	 * Method which takes a User object from the session,
	 * who is logged in.
	 * @return User - logged-in user
	 */
	public User getLoggedUser() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User user = ((UserPrincipal) principal).getUser();
		return user;
	}
		
	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}



}
