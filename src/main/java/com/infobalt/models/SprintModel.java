package com.infobalt.models;

import com.infobalt.entities.Sprint;

public class SprintModel {

	private Sprint currentSprint;

	public Sprint getCurrentSprint() {
		return currentSprint;
	}

	public void setCurrentSprint(Sprint currentSprint) {
		this.currentSprint = currentSprint;
	}

}
