package com.infobalt.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.event.SelectEvent;

import com.infobalt.cached.CachedTask;
import com.infobalt.entities.Project;
import com.infobalt.entities.Sprint;
import com.infobalt.entities.Task;
import com.infobalt.entities.User;
import com.infobalt.enums.TaskStatus;
import com.infobalt.models.ProjectModel;
import com.infobalt.models.TaskModel;
import com.infobalt.models.UserModel;
import com.infobalt.services.TaskService;

public class TaskController {
	
	private static final Logger logger = LogManager.getLogger(UserController.class.getName());

	private TaskModel taskModel;
	private TaskService taskService;
	private ProjectModel projectModel;
	private UserModel userModel;
	private CachedTask cachedTask;
	
	public String create(){
		Task newTask = new Task();
		newTask.setCreator(userModel.getLoggedUser());
		newTask.setProject(projectModel.getCurrentProject());
		newTask.setCreationDate(new Date());
		newTask.setUpdateDate(new Date());
		newTask.setTaskStatus(TaskStatus.TODO);
		taskModel.setCurrentTask(newTask);
		return "addTask";
	}
	
	public String update(Task task){
		taskModel.setCurrentTask(task);
		return "task-update";
	}
	
	public String save(){
		taskService.save(taskModel.getCurrentTask());
		if(taskModel.getCurrentTask().getId() == null){
			logger.info("New task: " + "'" + taskModel.getCurrentTask().getTitle() + "'" + " was created by: " 
					+ "'" + userModel.getLoggedUser().showFullName() + "'");
		}
		else {
			logger.info("Task: " + "'" + taskModel.getCurrentTask().getTitle() + "'" + "was updated by: "
					+ "'" + userModel.getLoggedUser().showFullName() + "'");
		}
		return "saveTask";
	}
	
	public void save(Task task){
		taskService.save(task);
		cachedTask.reset();
	}
	
	public String cancel(){
		taskModel.setCurrentTask(null);
		return "cancel";
	}
	
	public void delete(Task task){
		taskService.delete(task);
	}
	
	public String overview(Task task){
		taskModel.setCurrentTask(task);
		return "overviewTask";
	}
	/**
	 * This method displays/renders UI component, if logged user is project worker, project manager or admin.
	 * @return returns true - if UI component is displayed
	 */
	public boolean displayItemToAuthorisedUser(){
		Project project = taskModel.getCurrentTask().getProject();
		List<User> users =  project.getUsers();
		User loggedUser = userModel.getLoggedUser();

		//display always to admin or project manager
		if(project.getProjectManager().getId() == loggedUser.getId() || loggedUser.getRole().getRole().equals("admin")){
			return true;
		}
		
		if(users != null && users.size() != 0){
			for(User u : users){
				if(u.getId().equals(loggedUser.getId())){
					return true;
				}
			}
		}
		
		return false;
	}
	/**
	 * This method displays project view page, if logged user is project worker, project manager, admin
	 * or logged user not participate in project workers, but have tasks with status DONE, assigned to him. 
	 * @return returns true - if page is displayed
	 */
	public boolean displayPage(){
		List<User> users = new ArrayList<User>();
		Project project = null;
		User loggedUser = null;
		Task currentTask = null;
		
		try {
			project = taskModel.getCurrentTask().getProject();
			users =  project.getUsers();
			loggedUser = userModel.getLoggedUser();
			currentTask = taskModel.getCurrentTask();
		
			//display always to admin or project manager
			if(project.getProjectManager().getId() == loggedUser.getId() || loggedUser.getRole().getRole().equals("admin")){
				return true;
			}
		
			if(users != null && users.size() != 0){
				for(User u : users){
					//Display page:
					//if logged user is assigned to task, and logged user not participate in project workers, and task status is DONE 
					if(loggedUser.getId().equals(currentTask.getAssignee().getId()) && !u.getId().equals(loggedUser.getId()) && currentTask.getTaskStatus() == TaskStatus.DONE ){
						return true;
					}else if(u.getId().equals(loggedUser.getId())){//if logged user participate in project workers
						return true;
					}
				}
			}	
		} catch (NullPointerException e) {
			return false;
		}
				
		return false;
	}
	
	public void removeSprint(Task task){
		taskService.removeSprint(task);
		cachedTask.reset();
	}
	
	/**
	 * Adds task to a sprint;
	 * sets AddedToSprintDate to current date;
	 * @param task - task to be added
	 * @param sprint - sprint to which task will be added
	 */
	public void addToSprint(Task task, Sprint sprint){
		taskService.addToSprint(task, sprint);
		cachedTask.reset();
	}
	
	/**
	 * Sets AddedToSprintDate to current date;
	 * Used when there is need to set sprint and date
	 * before saving task to a DB
	 */
	public void assignToSprintDate(){
		taskModel.getCurrentTask().setAddedToSprintDate(new Date());
	}
	
	public void changeStatus(){
		taskService.changeStatus(taskModel.getCurrentTask(), userModel.getLoggedUser());
	}
	
	public void changeStatus(Task task){
		taskService.changeStatus(task, userModel.getLoggedUser());
		cachedTask.reset();
	}
	
	public List<Task> findUserTasks(User user){
		return taskService.findUserTasks(user);
	}
	
	public List<Task> findAll() {
		return taskService.findAll();
	}
	
	public Task findById(Integer id){
		return taskService.findById(id);
	}
	
	public List<Task> findByProject(Project project){
		return taskService.findByProject(project);
	}	
	
	public List<Task> findBySprint(Sprint sprint){
		return taskService.findBySprint(sprint);
	}
	
	public List<Task> findByProjectAndStatus(Project project, TaskStatus taskStatus){
		return taskService.findByProjectAndStatus(project, taskStatus);
	}
	
	public List<Task> findBySprintAndStatus(Sprint sprint, TaskStatus taskStatus){
		return taskService.findBySprintAndStatus(sprint, taskStatus);
	}
	
	public List<Task> findNotInSprint(Project project){
		return taskService.findNotInSprint(project);
	}
	/**
	 * Finds tasks with status TODO and INPROGRESS, assigned to suspended users, who participates in project.
	 */
	public List<Task> findSuspendedUsersTasksInProject(Project project){
		return taskService.findSuspendedUsersTasksInProject(project);
	}
	
	private void saveResetedTask(Task task){
		taskService.save(task);
	}
	/**
	 * This method sets task status to TODO and assignee to NULL of selected tasks
	 * @return navigation query
	 */
	public String resetSuspendedTasks(){
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		if(request.getParameterValues("resetTask") != null){
			for(Object val : request.getParameterValues("resetTask")){
				Task task = findById(Integer.valueOf(val.toString()));
				task.setAssignee(null);
				task.setTaskStatus(TaskStatus.TODO);
				saveResetedTask(task);
			}
		}
		return "resetTasks";
	}
	
	public String getStatusStyle(TaskStatus status) {
		if (status == null) {
			return "";
		}
		String styleClass = "taskstatus-container ";
		if (status == TaskStatus.TODO) {
			styleClass += "taskstatus-todo";
		} else if (status == TaskStatus.DONE) {
			styleClass += "taskstatus-done";
		} else {
			styleClass += "taskstatus-inprogress";
		}
		return styleClass;
	}
	
	public String searchTasksByFragment(String fragment){
		taskModel.setFragment(fragment);
		return "taskSearch";
	}
	
	/**
	 * Handles select event on datatable row,
	 * navigates to selected item page
	 * @param event SelectEvent
	 */
	public void onRowSelect(SelectEvent event) {
		taskModel.setCurrentTask((Task) event.getObject());
		ConfigurableNavigationHandler handler = (ConfigurableNavigationHandler) FacesContext.getCurrentInstance()
				.getApplication().getNavigationHandler();
		handler.performNavigation("overviewTask");
	}
	
	/**
	 * Method is used to load task using its id saved in taskModel
	 * (navigation with GET parameters - from drag and drop dashboard)
	 * Redirected to home path if task do not exist.
	 */
	public void loadTask(){
		if (taskModel.getId() != null){
			int id = Integer.parseInt(taskModel.getId());
			Task task = findById(id);
			
			if (task != null) {
				taskModel.setCurrentTask(task);
				taskModel.setId(null);
			} else {
				ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		        try {
					ec.redirect(ec.getRequestContextPath() + "/");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}			
	}
	
	public TaskModel getTaskModel() {
		return taskModel;
	}
	
	public void setTaskModel(TaskModel taskModel) {
		this.taskModel = taskModel;
	}
	
	public TaskService getTaskService() {
		return taskService;
	}
	
	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public ProjectModel getProjectModel() {
		return projectModel;
	}

	public void setProjectModel(ProjectModel projectModel) {
		this.projectModel = projectModel;
	}

	public UserModel getUserModel() {
		return userModel;
	}

	public void setUserModel(UserModel userModel) {
		this.userModel = userModel;
	}

	public CachedTask getCachedTask() {
		return cachedTask;
	}

	public void setCachedTask(CachedTask cachedTask) {
		this.cachedTask = cachedTask;
	}

	public List<TaskStatus> getTaskStatuses() {
		return Arrays.asList(TaskStatus.values());
	}
	
}
