package com.infobalt.controllers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.infobalt.cached.CachedTask;
import com.infobalt.entities.Project;
import com.infobalt.entities.Task;
import com.infobalt.entities.User;
import com.infobalt.enums.TaskStatus;
import com.infobalt.models.ProjectModel;
import com.infobalt.models.UserModel;
import com.infobalt.services.ProjectService;
import com.infobalt.services.TaskService;

public class ProjectController {
	
	private static final Logger logger = LogManager.getLogger(UserController.class.getName());

	private ProjectModel projectModel;
	private ProjectService projectService;
	private TaskService taskService;
	private CachedTask cachedTask;
	private UserModel userModel;

	public String create() {
		projectModel.setCurrentProject(new Project());
		return "addProject";
	}

	public String update(Project project) {
		projectModel.setCurrentProject(project);
		return "project-update";
	}

	/**
	 * This method Assigns project manager to project workers, only, when new project is created. 
	 */
	public void saveWithAutoAssign(){
		if(projectModel.getCurrentProject().getProjectManager() != null){
			if(projectModel.getCurrentProject().getUsers() == null){
				List<User> projectManager = new ArrayList<User>(); 
				projectManager.add(projectModel.getCurrentProject().getProjectManager());
				projectModel.getCurrentProject().setUsers(projectManager);
			}
		}
		save();
	}
	
	public String save() {
		projectService.save(projectModel.getCurrentProject());
		if(projectModel.getCurrentProject().getId() == null){
			logger.info("New project " + "'" + projectModel.getCurrentProject().getTitle() + "'" + " was created by: " 
					+ "'" + userModel.getLoggedUser().showFullName() + "'");
		}
		else {
			logger.info("Project " + "'" + projectModel.getCurrentProject().getId() + "'" + " was updated by: " 
					+ "'" + userModel.getLoggedUser().showFullName() + "'");
		}
		return "save";
	}
	/**
	 * This method checks if user is unselected from project workers, converts event object to User
	 * and gives it to 'resetTasksByUserInProject' method as parameter.
	 * @param event unselect user event
	 */
	public void onUserUnselect(UnselectEvent event){
		User user = (User)event.getObject();
		resetTasksByUserInProject(user);
	}
	/**
	 *  This method checks if user has unfinished tasks in the project and sets tasks status to TODO and assignee to NULL.
	 * @param user - user to check if has unfinished tasks
	 */
	public void resetTasksByUserInProject(User user){ 
		List<Task> tasks = taskService.findByProjectAndUserWithStatusNotDone(projectModel.getCurrentProject(), user);
		if(tasks.size() != 0){
			for(Task t : tasks){
				t.setAssignee(null);
				t.setTaskStatus(TaskStatus.TODO);
				taskService.save(t);
			}
		}
		save();
	}
	/**
	 * This method displays project view page, if logged user is project worker, project manager or admin. 
	 * @return returns true - if page is displayed.
	 */
	public boolean displayPage(){
		User loggedUser = null;
		List<User> users = null;
				
		try {
			loggedUser = userModel.getLoggedUser();
			users = projectModel.getCurrentProject().getUsers();
			
			//display always to admin or project manager
			if(projectModel.getCurrentProject().getProjectManager().getId() == loggedUser.getId() || loggedUser.getRole().getRole().equals("admin")){
				return true;
			}
			
			if(users != null && users.size() != 0){
				for(User u : users){
					if(u.getId().equals(loggedUser.getId())){
						return true;
					}
				}
			}	
		} catch (NullPointerException e) {
			return false;
		}
				
		return false;
	}
	
	public String cancel() {
		projectModel.setCurrentProject(null);
		return "cancel";
	}

	public void delete(Project project) {
		projectService.delete(project);
	}

	public String overview(Project project) {
		projectModel.setCurrentProject(project);
		return "overviewProject";
	}

	public List<Project> findUserManagedProjects(User user) {
		return projectService.findUserManagedProjects(user);
	}

	public Project findById(Integer id) {
		return projectService.findById(id);
	}
	
	/**
	 * Method check if the given user is a manager of current project.
	 * @param user
	 * @return boolean value of true if user is a manager of current project and false if not
	 */
	public boolean isManager(User user) {
		if (user != null) {
			if (projectModel.getCurrentProject() != null) {
				if (projectModel.getCurrentProject().getProjectManager() != null) {
					if (projectModel.getCurrentProject().getProjectManager().getId() == user.getId()) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Counts number of tasks in current project
	 * @return int Number of tasks
	 */
	public int countTasks() {
		return taskService.countTasks(projectModel.getCurrentProject());
	}

	/**
	 * Counts number of tasks with taskStatus.DONE in current project
	 * @return int Number of tasks
	 */
	public int countDoneTasks() {
		return taskService.countDoneTasks(projectModel.getCurrentProject());
	}

	/**
	 * Counts number of tasks where taskStatus is not equal to DONE in current project
	 * @return int Number of tasks
	 */
	public int countTasksLeft() {
		return taskService.countTasksLeft(projectModel.getCurrentProject());
	}

	/**
	 * Calculates number of done story points in current project
	 * @return int Story points
	 */
	public int countStoryPointsDone() {
		return projectService.countStoryPointsDone(projectModel.getCurrentProject());
	}

	/**
	 * Calculates number of story points still left to be done in current project
	 * @return int Story points
	 */
	public int countStoryPointsLeft() {
		return projectService.countStoryPointsLeft(projectModel.getCurrentProject());
	}

	/**
	 * Returns progress of current project by calculating ratio of story points
	 * sum of tasks with status DONE and tasks with statuses INPROGRESS and TODO
	 * @return double Progress
	 */
	public double getProgress() {
		return projectService.getProgress(projectModel.getCurrentProject());
	}

	/**
	 * Calculates task velocity of tasks accomplishment in current project.
	 * @return double Velocity
	 */
	public double getTasksPerDay() {
		return projectService.getTasksPerDay(projectModel.getCurrentProject());
	}

	/**
	 * Calculates task velocity of story points accomplishment in current
	 * project.
	 * @return double Velocity
	 */
	public double getStoryPointsPerDay() {
		return projectService.getStoryPointsPerDay(projectModel.getCurrentProject());
	}

	/**
	 * Returns working days, that is the number of days between the start date
	 * and finish date or current date, depending whether project is finished or
	 * still in progress
	 * @return int project duration in days
	 */
	public int workingTime() {
		return projectService.workingTime(projectModel.getCurrentProject());
	}
	
	/**
	 * Exports current project backlog tasks to .csv file.
	 * @return streamed content of generated file.
	 * @throws FileNotFoundException
	 */
	public StreamedContent getBacklogCsvFile() throws FileNotFoundException{
		List<Task> taskList = cachedTask.getTasksNotInSprint(projectModel.getCurrentProject());
		return generateCsvFile(taskList);
	}
	
	/**
	 * Exports all current project tasks to .csv file.
	 * @return streamed content of generated file.
	 * @throws FileNotFoundException
	 */
	public StreamedContent getProjectTasksCsvFile() throws FileNotFoundException{
		List<Task> taskList = cachedTask.getProjectTasks(projectModel.getCurrentProject());
		return generateCsvFile(taskList);
	}
	
	/**
	 * Generates and exports tasks to .csv file.
	 * @param list of tasks to be exported into a file
	 * @return streamed content of generated file.
	 * @throws FileNotFoundException if file was not found.
	 */
	public StreamedContent generateCsvFile(List<Task> taskList) throws FileNotFoundException{
		String tmpdir = System.getProperty("java.io.tmpdir");
		File file = null;

		BufferedWriter writer = null;
		
		StreamedContent download = null;
	    InputStream inputStream = null;
	    ExternalContext externalContext = null;
		//generate .csv file
		try {
			file = new File(tmpdir+"/"+ projectModel.getCurrentProject().getTitle() +".csv");
			writer = new BufferedWriter(new FileWriter(file));
			
			writer.write("ID");
			writer.write(',');
			writer.write("Title");
			writer.write(',');
			writer.write("StoryPoints");
			writer.write(',');
			writer.write("Assigned to");
			writer.write(',');
			writer.write("Status");
			writer.write(',');
			writer.write("Sprint");
			writer.write(',');
			writer.write("Created");
			writer.write(',');
			writer.write("Updated");
			writer.write(',');
			writer.write("Completed");
			writer.newLine();
			
			for(Task task : taskList){
				writer.write(validateWriter(task.getId()));
				writer.write(',');
				writer.write(validateWriter(task.getTitle()));
				writer.write(',');
				writer.write(validateWriter(String.valueOf(task.getStoryPoints())));
				writer.write(',');
				try {
					writer.write(validateWriter(task.getAssignee().showFullName()));
				} catch (NullPointerException e) {
					writer.write("");
					e.getMessage();
				}
				writer.write(',');
				writer.write(validateWriter(task.getTaskStatus().getValue()));
				writer.write(',');
				try {
					writer.write(validateWriter(task.getSprint().getTitle()));
				} catch (NullPointerException e) {
					writer.write("");
					e.getMessage();
				}
				writer.write(',');
				writer.write(validateWriter(task.getCreationDate()));
				writer.write(',');
				writer.write(validateWriter(task.getUpdateDate()));
				writer.write(',');
				writer.write(validateWriter(task.getCompleteDate()));
				writer.newLine();
			}
		}catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	    //export .csv file
	    try {
	    	download = new DefaultStreamedContent();
	    	inputStream = new FileInputStream(file);
	    	externalContext = FacesContext.getCurrentInstance().getExternalContext();
	    	download = new DefaultStreamedContent(inputStream, externalContext.getMimeType(file.getName()), file.getName());
		} catch (Exception e) {
			e.printStackTrace();
		}

	    return download;
	}
	/**
	 * Validates if object is not null, if so, returns an empty string
	 * @param value - given object
	 * @return - object converted to string
	 */
	private String validateWriter(Object value){
		try {
			if(value != null){
				return value.toString();
			}
		} catch (NullPointerException e) {
			e.getMessage();
		}
		return "";
	}
	
	/**
	 * Handles select event on datatable row,
	 * navigates to selected item page
	 * @param event SelectEvent
	 */
	public void onRowSelect(SelectEvent event) {
		projectModel.setCurrentProject((Project) event.getObject());
		ConfigurableNavigationHandler handler = (ConfigurableNavigationHandler) FacesContext.getCurrentInstance()
				.getApplication().getNavigationHandler();
		handler.performNavigation("overviewProject");
	}

	public ProjectModel getProjectModel() {
		return projectModel;
	}

	public void setProjectModel(ProjectModel projectModel) {
		this.projectModel = projectModel;
	}

	public ProjectService getProjectService() {
		return projectService;
	}

	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public CachedTask getCachedTask() {
		return cachedTask;
	}

	public void setCachedTask(CachedTask cachedTask) {
		this.cachedTask = cachedTask;
	}

	public UserModel getUserModel() {
		return userModel;
	}

	public void setUserModel(UserModel userModel) {
		this.userModel = userModel;
	}

}
