package com.infobalt.controllers;

import java.io.ByteArrayInputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.UploadedFile;

import com.infobalt.entities.Attachment;
import com.infobalt.models.AttachmentModel;
import com.infobalt.models.TaskModel;
import com.infobalt.models.UserModel;
import com.infobalt.services.AttachmentService;

public class AttachmentController {

	private static final Logger logger = LogManager.getLogger(UserController.class.getName());

	private AttachmentModel attachmentModel;
	private AttachmentService attachmentService;
	private TaskModel taskModel;
	private UserModel userModel;

	/**
	 * Method handles file upload event. It takes selected file and writes it into storage.
	 * @param event
	 */
	public void handleFileUpload(FileUploadEvent event) {
		Attachment attachment = new Attachment();
		attachment.setTask(taskModel.getCurrentTask());
		UploadedFile file = event.getFile();
		attachment.setName(file.getFileName());
		attachment.setData(file.getContents());
		attachment.setMimeType(file.getContentType());
		attachmentService.save(attachment);
		logger.info("User: " + "'" + userModel.getLoggedUser().showFullName() + "'" 
				+ " uploaded file: " + "'" + attachment.getName() + "'"
				+ " on task: " + "'" + taskModel.getCurrentTask().getId() + "'");
	}
	
	/**
	 * Method prepares the attachment for download. Based on given attachmentId it extracts the file from storage.
	 * @param attachmentId
	 */
	public void prepareDownloadFile(Integer attachmentId) {
		Attachment attachment = attachmentService.findById(attachmentId);
		ByteArrayInputStream stream = new ByteArrayInputStream(attachment.getData());
		String contentType = attachment.getMimeType();
		attachmentModel.setDownloadFile(new DefaultStreamedContent(stream, contentType, attachment.getName()));
	}

	public AttachmentModel getAttachmentModel() {
		return attachmentModel;
	}

	public void setAttachmentModel(AttachmentModel attachmentModel) {
		this.attachmentModel = attachmentModel;
	}

	public AttachmentService getAttachmentService() {
		return attachmentService;
	}

	public void setAttachmentService(AttachmentService attachmentService) {
		this.attachmentService = attachmentService;
	}

	public TaskModel getTaskModel() {
		return taskModel;
	}

	public void setTaskModel(TaskModel taskModel) {
		this.taskModel = taskModel;
	}

	public UserModel getUserModel() {
		return userModel;
	}

	public void setUserModel(UserModel userModel) {
		this.userModel = userModel;
	}

}
