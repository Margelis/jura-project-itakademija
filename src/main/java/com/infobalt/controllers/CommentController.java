package com.infobalt.controllers;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.infobalt.cached.CachedComment;
import com.infobalt.entities.Comment;
import com.infobalt.entities.Task;
import com.infobalt.models.CommentModel;
import com.infobalt.models.TaskModel;
import com.infobalt.models.UserModel;
import com.infobalt.services.CommentService;
import com.infobalt.services.TaskService;

public class CommentController {
	
	private static final Logger logger = LogManager.getLogger(UserController.class.getName());

	static final long ONE_MINUTE_IN_MILLIS = 60000;
	
	private CommentModel commentModel;
	private CommentService commentService;
	private TaskService taskService;
	private TaskModel taskModel;
	private UserModel userModel;
	private CachedComment cachedComment;
	
	public String create(){
		commentModel.setCurrentComment(new Comment());
		commentModel.getCurrentComment().setTask(taskModel.getCurrentTask());
		commentModel.getCurrentComment().setUser(userModel.getLoggedUser());
		commentModel.getCurrentComment().setCommentDate(new Date());
		return "comment-create";
	}
	
	public String update(Comment comment){
		commentModel.setCurrentComment(comment);
		return "comment-update";
	}
	
	
	public void save(){
		commentService.save(commentModel.getCurrentComment());
		logger.info("User: " + "'" + userModel.getLoggedUser().showFullName() + "'" + " commented" + " on task: " 
				+ "'" + taskModel.getCurrentTask().getId() + "'");
	}
	
	public void cancel(){
		commentModel.setCurrentComment(null);
	}
	
	public void delete(Comment comment){
		if(showDeleteButton(comment) == true){
			commentService.delete(comment);
			cachedComment.reset();
			logger.info("Comment on task: " + "'" + taskModel.getCurrentTask().getId() + "'" 
					+ " was deleted by: " + userModel.getLoggedUser().showFullName());
		}
	}
	
	public String overview(Comment comment){
		commentModel.setCurrentComment(comment);
		return "comment-overview";
	}
	
	public List<Comment> findAll() {
		return commentService.findAll();
	}
	
	public Comment findById(Integer id){
		return commentService.findById(id);
	}
	
	public List<Comment> findByTask(Task task) {
		return commentService.findByTask(task);
	}

	public boolean showDeleteButton(Comment comment){
		Calendar date = Calendar.getInstance();
		long t = date.getTimeInMillis();
		String loggedInUser = userModel.getLoggedUser().getRole().getRole();
		Date dateAfterRemovingOneMinute = new Date(t - ONE_MINUTE_IN_MILLIS);
			if(loggedInUser.equals("admin")){ //ADMIN
				return true;
			} 
			else if(dateAfterRemovingOneMinute.before(comment.getCommentDate())){
				if(comment.getUser().getLogin() == userModel.getLoggedUser().getLogin()){
					return true;
				}
			}
			return false;
		}

	public CommentModel getCommentModel() {
		return commentModel;
	}

	public void setCommentModel(CommentModel commentModel) {
		this.commentModel = commentModel;
	}

	public CommentService getCommentService() {
		return commentService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public TaskModel getTaskModel() {
		return taskModel;
	}

	public void setTaskModel(TaskModel taskModel) {
		this.taskModel = taskModel;
	}

	public UserModel getUserModel() {
		return userModel;
	}

	public void setUserModel(UserModel userModel) {
		this.userModel = userModel;
	}
	
	public CachedComment getCachedComment() {
		return cachedComment;
	}
	
	public void setCachedComment(CachedComment cachedComment) {
		this.cachedComment = cachedComment;
	}
}
