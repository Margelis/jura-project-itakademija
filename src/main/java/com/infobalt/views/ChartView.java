package com.infobalt.views;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

import com.infobalt.cached.CachedTask;
import com.infobalt.controllers.SprintController;
import com.infobalt.entities.Task;
import com.infobalt.models.SprintModel;

/**
 * Class for calculations and control of Burndown Chart.
 * @author Zygimantas
 *
 */
public class ChartView implements Serializable{
	
	private static final long serialVersionUID = 3630283884732226273L;
	
	private LineChartModel dateModel;
	private SprintController sprintController;
	private CachedTask cachedTask;
	private SprintModel sprintModel;
	private Map<String, Integer> spMap = new TreeMap<String, Integer>();
	private DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    public void init() {
    	if(sprintModel.getCurrentSprint() != null){
    		createDateModel();
    	}
    }
    
    /**
     * Creates model; calculates and sets values to model
     */
    private void createDateModel() {
    	//create model
        dateModel = new LineChartModel();
        
        // create series
        LineChartSeries series1 = new LineChartSeries();
        series1.setLabel("Guideline");
        series1.setShowMarker(false);
 
        LineChartSeries series2 = new LineChartSeries();
        series2.setLabel("Remaining values");
        series2.setShowMarker(false);
        
        
        List<Task> allTasks = cachedTask.getTasksInSprint(sprintModel.getCurrentSprint());
        Date sprintStartDate = sprintModel.getCurrentSprint().getStartDate(); //sprint start date
        Date sprintFinishDate = sprintModel.getCurrentSprint().getFinishDate(); // sprint finish date
        int sprintInitialStoryPoints = 0;
         
        //iterate through tasks
        for (Task task: allTasks){
        	
        	if (task.getCompleteDate() != null){
        		
        		if (task.getAddedToSprintDate().after(task.getCompleteDate())) {
        			continue;
        		}
        		
        		if (task.getCompleteDate().after(sprintStartDate) && task.getCompleteDate().before(sprintFinishDate)){
        			populateMap(task.getCompleteDate(), 0 - task.getStoryPoints());
        		}
        	}
        	
        	if (isSameDay(task.getAddedToSprintDate(), sprintStartDate) || !task.getAddedToSprintDate().after(sprintStartDate)) {
        		sprintInitialStoryPoints = sprintInitialStoryPoints + task.getStoryPoints();
        		
        	} else if (task.getAddedToSprintDate().before(sprintFinishDate)){
        		populateMap(task.getAddedToSprintDate(), task.getStoryPoints());
        	}	
        	
        }
        populateMap(sprintStartDate, 0);
        
        //set guideline values
        series1.set(df.format(sprintStartDate), sprintInitialStoryPoints);
        series1.set(df.format(sprintFinishDate), 0);
        
        //iterates through map and sets values to series
        for (String date: spMap.keySet()){
        	sprintInitialStoryPoints = sprintInitialStoryPoints + spMap.get(date);
        	series2.set(date, sprintInitialStoryPoints);
        }
        
        // the last date in the chart
        Date lastDate;
        if (new Date().getTime() > sprintFinishDate.getTime()){
        	lastDate = sprintFinishDate;
        } else {
        	lastDate = new Date();
        }
        //set the last date
        series2.set(df.format(lastDate), sprintInitialStoryPoints);
        
 
        dateModel.addSeries(series1);
        dateModel.addSeries(series2);
        dateModel.setSeriesColors("0277BD,ff1a1a");
        dateModel.setExtender("customChartExtender");
        dateModel.setLegendPosition("e");
        dateModel.getAxis(AxisType.Y).setLabel("STORY POINTS");
        dateModel.getAxis(AxisType.Y).setMin(0);
        DateAxis axis = new DateAxis("TIME");
        axis.setTickAngle(-50);
        axis.setTickFormat("%b %#d");
        axis.setMin(df.format(sprintStartDate));
        axis.setMax(df.format(sprintFinishDate));
        dateModel.getAxes().put(AxisType.X, axis);
    }
    
    /**
     * Populates map that is used for model values
     * @param date Date of the change
     * @param points Story points to be added/subtracted to the date
     */
    public void populateMap(Date date, int points){
		if (spMap.containsKey(df.format(date))){
    		int value = spMap.get(df.format(date));
			spMap.put(df.format(date), value + points);
			
		} else {
			spMap.put(df.format(date), points);
		}
    }
    
    /**
     * Checks if dates are the same (accuracy is one calendar day)
     * @param date1 first Date
     * @param date2 second Date
     * @return boolean true, if dates are the same
     */
    public boolean isSameDay(Date date1, Date date2) {
    	Calendar cal1 = Calendar.getInstance();
    	Calendar cal2 = Calendar.getInstance();
    	cal1.setTime(date1);
    	cal2.setTime(date2);
    	boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
    	                  cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
    	return sameDay;
    }
    
	public LineChartModel getDateModel() {
		return dateModel;
	}

	public SprintController getSprintController() {
		return sprintController;
	}

	public void setSprintController(SprintController sprintController) {
		this.sprintController = sprintController;
	}

	public CachedTask getCachedTask() {
		return cachedTask;
	}

	public void setCachedTask(CachedTask cachedTask) {
		this.cachedTask = cachedTask;
	}

	public SprintModel getSprintModel() {
		return sprintModel;
	}

	public void setSprintModel(SprintModel sprintModel) {
		this.sprintModel = sprintModel;
	}
	
	

}
