package com.infobalt.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Class which encodes password
 * @author Zygimantas
 *
 */
public final class PasswordEncoder {
	
	/**
	 * Method used to encode given password
	 * @param password - a plain text String which is to be hashed
	 * @return - a hashed String which is to be stored to a DB
	 */
	public static String encodePassword(String password){
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(password);
		return hashedPassword;
	}
}
