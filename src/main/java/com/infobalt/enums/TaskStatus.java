package com.infobalt.enums;

public enum TaskStatus {
	TODO("To Do"),
	INPROGRESS("In Progress"),
	DONE("Done");
	
	private String value;
	
	private TaskStatus(String value){
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}

}
