package com.infobalt.validators;

import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.infobalt.entities.Project;
import com.infobalt.entities.Sprint;
import com.infobalt.models.ProjectModel;
import com.infobalt.services.SprintService;

/**
 * Custom validator for date ranges (
 * second date must be after first date)
 * @author Zygimantas
 *
 */
public class ProjectDatesValidator implements Validator{
	
	private ProjectModel projectModel;
	private SprintService sprintService;

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        
		if (value == null) {
            return; // Let required="true" handle.
        }
		
        Date startDate;
        Date endDate;
        Project currentProject = projectModel.getCurrentProject();
        
        
        //Check which component invoked validator and assign start and end values
        if (component.getId().equals("start")){
        	startDate = (Date) value;
        	endDate = currentProject.getFinishDate();
        	
        } else if (component.getId().equals("finish")){
        	startDate = currentProject.getStartDate();
        	endDate = (Date) value;
        	
        } else if (component.getId().equals("finishDateAdd")){
        	UIInput startDateComponent = (UIInput) component.getAttributes().get("startDateComponent");
        	
        	if (!startDateComponent.isValid()) {
                return; // Already invalidated. Don't care about it then.
            }
        	
        	startDate = (Date) startDateComponent.getValue();
        	
            if (startDate == null) {
                return; // Let required="true" handle.
            }
            
            endDate = (Date) value;
            
        } else {
        	return;
        }       
        
        // check if end date is not before start date
        if (endDate.before(startDate)){
        	throw new ValidatorException(new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "Start date cannot be after end date.", null));
        }
        
        // check if end or start date do not conflict with sprint dates
        if (projectModel.getCurrentProject().getId() != null) {
	        List<Sprint> sprints = sprintService.findByProject(projectModel.getCurrentProject());
	        if (sprints != null) {
	        	Date firstSprintStartDate = new Date(Long.MAX_VALUE);
	        	Date lastSprintEndtDate = new Date(0);
	        	Date sprintStartDate;
	        	Date sprintEndtDate;
	        	for (Sprint sprint : sprints) {
	        		sprintStartDate = sprint.getStartDate();
	        		if (sprintStartDate.compareTo(firstSprintStartDate) < 0) {
	        			firstSprintStartDate = sprintStartDate;
	        		}
	        		sprintEndtDate = sprint.getFinishDate();
	        		if (sprintEndtDate.compareTo(lastSprintEndtDate) > 0) {
	        			lastSprintEndtDate = sprintEndtDate;
	        		}
	        	}
	        	if (startDate.compareTo(firstSprintStartDate) > 0) {
	        		throw new ValidatorException(new FacesMessage(
	        				FacesMessage.SEVERITY_ERROR, "Project cannot be started later than the first sprint.", null));
	        	}
	        	if (endDate.compareTo(lastSprintEndtDate) < 0) {
	        		throw new ValidatorException(new FacesMessage(
	        				FacesMessage.SEVERITY_ERROR, "Project cannot be ended earlier than the last sprint.", null));
	        	}
	        }
        }
		
	}

	public ProjectModel getProjectModel() {
		return projectModel;
	}

	public void setProjectModel(ProjectModel projectModel) {
		this.projectModel = projectModel;
	}
	
	public SprintService getSprintService() {
		return sprintService;
	}
	
	public void setSprintService(SprintService sprintService) {
		this.sprintService = sprintService;
	}
	
}
